// Arrays
    // Arrays adalah variabel yang dapat menampung beberapa value didalamnya

    const numbers = new Array(1,2,3,4,5);
    const fruits = ['apples', 'oranges', 'pears'];
    
    console.log(numbers);
    console.log(fruits);
        // code dibawah ditambahkan index dari array
    console.log(fruits[1]);
        // code dibawah untuk menambahkan urutan array terbaru
    fruits[3] = 'grapes';
    // code dibawah untuk menambahkan urutan array terbaru dari terakhir
    fruits.push('mangos');
    // code dibawah untuk menambahkan urutan array terbaru dari pertama
    fruits.unshift('strawberries');
    // code dibawah untuk menghapus array dari urutan terakhir
    fruits.pop();
    console.log(fruits);
    console.log(Array.isArray(fruits));
    console.log(fruits.indexOf('oranges'));