// VARIABEL 

    // ada 3 cara penulisan variabel yaitu var, let, const :

    // variabel dengan var memiliki scope secara global yang dimana kita dapat menuliskan
    // nama variabel yang sama tetapi hal itu dapat menyebabkan error

    // untuk variabel let, menggunakan nama variabel yang sama tetapi valuenya memaki yang terbaru ditulis

    let age = 24;

    age = 25


    console.log(age);
    // dari code diatas maka akan menampilkan age 31 bukan 30 karena dia menimpa value sebelumnya


    // untuk variabel const, tidak dapat menggunakan nama variabel yang sama 

    const umur = 30;

    // umur = 31;

    console.log(umur)
    // code diatas akan terjadi error dikarenakan variabel const tidak bisa memiliki nama yang sama