
// For

const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text:'Dentist appt',
        isCompleted: false
    }
];

const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
    return todo.text;
});

for(let i = 0; i < todos.length; i++) {
    console.log(todos[i]);
    console.log(todos[i].id);
    console.log(todos[i].text);
    console.log(todos[i].isCompleted);
};

console.log(todoCompleted);

for(let i = 0; i < 10; i++) {
    console.log(`For Loop Number: ${i}`);
}

// While

let i = 0;

while(i <= 10) {
  console.log(`While Loop Number: ${i}`);
  i++;
}