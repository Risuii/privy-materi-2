// DATA TYPES

    // String

    const nama = 'Faris';
        // data string selalu diawal dan diakhiri dengan kutip 2 atau kutip 1 dan lalu isi dari string itu

    // Number/Integer

    const angka = 20;
    const rating = 4.5
        // data Number/Integer adalah data yang berupa angka dan tidak perlu ada kutip 1 atau kutip 2 di awal atau diakhir

    // Boolean

    const isCool = true;
        // data Boolean terdiri dari 2 yaitu true atau false dan tidak perlu ada kutip 1 atau kutip 2 di awal atau diakhir

    // Null

    const x = null;
        // data Null artinya value dari variabel itu kosong 

    // Undefined

    const y = undefined;
        // data Undefined artinya value dari variabel itu tidak ditemukan

console.log(typeof nama);

// Concatenation
    // cara memanggil value dari variabel 
    console.log('My name is ' + nama + ' and I am ' + age);

    // Template String
        // memanggil variabel di dalam string
    const hello = `My Name is ${nama} and I am ${age}`;
    console.log(hello);
    
    // code dibawah menghitung berapa character dari sebuah kata
    
    const s = 'Hello World!';
    
    console.log(s.length);
    
    // code dibawah untuk Membuat semua huruf pada string jadi capital 
    
    const a = 'Hello World!';
    
    console.log(a.toUpperCase());
    
    // code dibawah untuk mencetak huruf yang diawali dari urutan ke 0 sampai ke 5
    
    const b = 'Hello World!';
    
    console.log(b.substring(0, 5).toUpperCase());
    
    // code dibawah untuk mencetak huruf satu per satu dan di jadikan array
    
    const c = 'Hello World!';
    const d = 'technology, computers, it, code';
    
    console.log(c.split(''));
    console.log(d.split(','))