package main

import "fmt"

func main() {
	var conferenceName = "Go Conference"
	const conferenceTickets = 50
	var remainingTickets = 50

	// fungsi \n untuk membuat line baru
	// dokumentasi untuk mengetahui %v dan sejenisnya https://pkg.go.dev/fmt

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available.\n", conferenceTickets, remainingTickets)

}
